"https://github.com/junegunn/vim-plug
call plug#begin()
Plug 'kien/ctrlp.vim'
Plug 'tomasr/molokai'
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'tpope/vim-surround'
Plug 'tpope/vim-fugitive'
Plug 'tpope/vim-repeat'
Plug 'Shougo/vimproc.vim', { 'do': 'make' }
Plug 'vimwiki/vimwiki', { 'branch': 'dev' }
Plug 'dhruvasagar/vim-table-mode'

Plug 'editorconfig/editorconfig-vim'

Plug 'leafgarland/typescript-vim'
Plug 'Quramy/tsuquyomi', { 'for': 'typescript' }
Plug 'dag/vim-fish'
Plug 'aklt/plantuml-syntax'
Plug 'm-kat/aws-vim'
call plug#end()

set ts=4
set sw=4
set sts=4
set autoindent
set expandtab
set smarttab
set smartcase
set incsearch
set hidden

colorschem molokai
set laststatus=2
set relativenumber
set number
set gcr=a:blinkon0
set scrolloff=3
set mouse=a
let g:airline_powerline_fonts = 1

let mapleader=','
nnoremap <leader>b :CtrlPBuffer<CR>
nnoremap <leader>e :CtrlP<CR>
nnoremap <leader>y "+y
nnoremap <leader>p "+p
nnoremap <leader>P "+P
vnoremap <leader>y "+y

nnoremap <F5> :w<CR> :silent make<CR>

let g:vimwiki_list = [{'path': '~/Documents/Notes/',
            \ 'syntax': 'markdown', 'ext': '.md'}]
let g:airline_theme='powerlineish'

let g:xml_syntax_folding=1
au FileType xml setlocal foldmethod=syntax
au FileType yaml setlocal ts=2 sts=2 sw=2 et ai

" osx
let g:python3_host_prog = '/usr/local/bin/python3'

set diffopt+=vertical
